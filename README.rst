Cyrano
######

Cyrano is an API experiment exploring a new way to write asynchronous network
code in Python. It exploits new features of Python 3 to provide a nicer
interface than usual for writing network code, without resorting to magic like
monkey-patching.
The default networking backend is based on asyncio, but backends are swappable.


Concept
=======

The name "Cyrano" helps to explain the concept. The name is from a character in the play titled *Cyrano de Bergerac.* In the play, the brilliant and huge-nosed Cyrano loves the brilliant and beautiful Roxane, but can't face her himself. 
So he courts her through a well-proportioned idiot named Christian: the idiot tells Cyrano the situation, Cyrano provides advice on what to do next, and the idiot follows the advice.

The key idea is to separate app logic from lower-level
protocols and concerns like asynchronous I/O. You write Cyrano apps using Python generators which don't directly do I/O. They don't even call a write() method. They take input and yield output. Their job is to contain the logic for what to do, but not to actually do it. So the programmer's app-generator plays the role of Cyrano: it knows all about what to do, but cannot actually do anything on its own. And the lower-level protocol and I/O layers play the role of the idiot.


Advantages
==========

What is the point of the idea? I think it has certain advantages:

* Apps are ultimately asynchronous, with the advantages that entails.
* The API for writing app-generators is unusually simple, almost transparent. 
* App logic in generators is synchronous, which makes it relatively easy to reason about and test in isolation. For example, it is very easy to simulate race conditions between yield points.
* The same app-generator can be reused on top of different lower-level protocols and networking backends with little fuss, since it is written with minimal assumptions about how it is to be used (it just provides advice). Generators are pretty composable.
* Yield points are explicit and there is no monkey-patching.


Examples
========

Here's a server which echoes back input in
uppercase until the connection is closed::

    def echo():
        last = b""
        while True:
            last = yield last.upper()
    

Here's a simple test of the app logic::

    def test_echo():
        it = echo()
        for message in [b'hi', b'there', b'bye']:
            expected = message.upper()
            output = it.send(message)
            assert output == expected
            
Here's a server with greeting and farewell messages::

    def hibye():
        said = yield b"Hi there. Send input and hit enter, would you?\n"
        output = "You sent {0!r}. Bye now!\n".format(said)
        return output.encode("utf-8")

Of course, most real life examples are not so simple, for example some will use shared state, many will use interesting protocols, and you are as always still responsible to make your own app work safely and do appropriate testing of the total system before deploying it. Network apps are an inherently complex domain, and Cyrano cannot be a silver bullet; it just tries to help with some of the complexity rather than making it worse with gratuitous API complexity.

See the ``demos/`` directory for more examples, notably including ones which
define the generators as methods on a class in order to share state.

Installing
==========

When this is uploaded to PyPI, installing will be a matter of using ``pip install cyrano``. For now, development can clone the repo and use ``pip install -e cyrano`` or whatever you would normally do (as long as it does not involve "sudo", and using
a Python3.4 virtualenv would be a good idea).


API Stability
=============

This is an experiment, that is reflected in the use of 0.x.x versions. It means that using Cyrano is a way of previewing or helping to build new things, and code written against Cyrano might have to be revised to take advantage of updates.
Only once the API becomes stable, that will be reflected in 1.x.x versioning.


Performance
===========

So far the priority is to refine the ideas, interface and overall design before
worrying about performance micro-optimizations.
There are already ways to write fast asynchronous apps - except they are a bit painful to use and may require using low-level languages.
Given this, it should be OK for Cyrano to specialize in being nicer to use, and put pure speed on the back burner. If I am happy with the way the model works, I would put higher priority on how performance scales than on absolute time numbers, to allow it to be used for bigger problems. 

So, concretely, ideas which make it harder to iterate, like using C extensions to speed up Cyrano itself, will not happen soon (if ever). It will be nice if PyPy begins to support Python 3.4 because it seems to be ideal for this kind of thing and could make it less interesting to resort to optimization strategies that involve C. On the other hand, some swappable backends might exploit C or whatever.
